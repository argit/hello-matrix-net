#!/bin/sh

CURL=/usr/bin/curl
GREP=/bin/grep
ECHO=/bin/echo
DATE=/bin/date

PROMETHEUS_URL=http://localhost:9090/api/v1/query
LIST_URL=https://www.hello-matrix.net/public_servers.php
SECRET_KEY=enter secret key here


# Print date
$DATE

# Get data from Prometheus

# SEND - LONG
$CURL -s "$PROMETHEUS_URL?query=sum(rate(monbot_ping_time_seconds_sum%5B14d%5D))%20without%20(receivingDomain)%20%2F%20sum(rate(monbot_ping_time_seconds_count%5B14d%5D))%20without%20(receivingDomain)%0A" | \
  $CURL -s --max-time 15 -X POST --data-binary @- "$LIST_URL?secret_key=$SECRET_KEY&monitor_data=send&monitor_period=long"

# SEND - SHORT
$CURL -s "$PROMETHEUS_URL?query=sum(rate(monbot_ping_time_seconds_sum%5B60m%5D))%20without%20(receivingDomain)%20%2F%20sum(rate(monbot_ping_time_seconds_count%5B60m%5D))%20without%20(receivingDomain)%0A" | \
  $CURL -s --max-time 15 -X POST --data-binary @- "$LIST_URL?secret_key=$SECRET_KEY&monitor_data=send&monitor_period=short"

# RECEIVE - LONG
$CURL -s "$PROMETHEUS_URL?query=sum(rate(monbot_ping_time_seconds_sum%5B14d%5D))%20without%20(sourceDomain)%20%2F%20sum(rate(monbot_ping_time_seconds_count%5B14d%5D))%20without%20(sourceDomain)%0A" | \
  $CURL -s --max-time 15 -X POST --data-binary @- "$LIST_URL?secret_key=$SECRET_KEY&monitor_data=receive&monitor_period=long"

# RECEIVE - SHORT
$CURL -s "$PROMETHEUS_URL?query=sum(rate(monbot_ping_time_seconds_sum%5B60m%5D))%20without%20(sourceDomain)%20%2F%20sum(rate(monbot_ping_time_seconds_count%5B60m%5D))%20without%20(sourceDomain)%0A" | \
  $CURL -s --max-time 15 -X POST --data-binary @- "$LIST_URL?secret_key=$SECRET_KEY&monitor_data=receive&monitor_period=short"

   
$ECHO Done.

# Print newlines
$ECHO 
$ECHO 
