SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE IF NOT EXISTS `error_log` (
  `error_id` int(11) NOT NULL AUTO_INCREMENT,
  `error_type` char(100) NOT NULL,
  `error_content` blob NOT NULL,
  `error_when` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`error_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `servers` (
  `hostname` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL,
  `category` varchar(50) NOT NULL,
  `location` varchar(5) NOT NULL,
  `online_since` int(11) NOT NULL,
  `server` varchar(50) NOT NULL,
  `notif_email` varchar(200) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

CREATE TABLE IF NOT EXISTS `server_room_count` (
  `hostname` varchar(200) NOT NULL,
  `current` tinyint(1) NOT NULL DEFAULT '0',
  `response_code` int(11) NOT NULL,
  `response_time` float NOT NULL,
  `public_room_count` int(11) NOT NULL,
  `measured_from` varchar(50) NOT NULL,
  `measured_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `hostname_current` (`hostname`,`current`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `server_security` (
  `hostname` varchar(200) NOT NULL,
  `current` tinyint(1) NOT NULL,
  `grade` varchar(5) NOT NULL,
  `gradeTrustIgnored` varchar(5) NOT NULL,
  `hasWarnings` tinyint(1) NOT NULL,
  `measured_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `hostname` (`hostname`,`current`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `server_statistics` (
  `hostname` varchar(200) NOT NULL,
  `last_response` int(11) NOT NULL,
  `last_response_time` float DEFAULT NULL,
  `status_since` datetime DEFAULT NULL,
  `last_versions` varchar(500) NOT NULL,
  `measured_from` varchar(50) NOT NULL,
  `measurements` int(11) NOT NULL,
  `successful` int(11) NOT NULL,
  `sum_response_time` double NOT NULL,
  `measurements_short` int(11) NOT NULL,
  `successful_short` int(11) NOT NULL,
  `sum_response_time_short` double NOT NULL,
  PRIMARY KEY (`hostname`,`measured_from`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `server_timings` (
  `hostname` varchar(200) NOT NULL,
  `response_code` int(11) NOT NULL,
  `response_time` float NOT NULL,
  `versions` varchar(500) NOT NULL,
  `measured_from` varchar(50) NOT NULL,
  `measured_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `hostname_measured_from` (`hostname`,`measured_from`) USING BTREE,
  KEY `measured_from_when` (`measured_from`,`measured_when`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `server_users` (
  `hostname` varchar(200) NOT NULL,
  `current` tinyint(1) NOT NULL,
  `users_active` int(11) NOT NULL,
  `users_deactivated` int(11) NOT NULL,
  `users_bridged` int(11) DEFAULT NULL,
  `guests` int(11) NOT NULL,
  `from_remote_addr` varchar(100) NOT NULL,
  `at_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `hostname` (`hostname`,`current`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `server_version` (
  `hostname` varchar(200) NOT NULL,
  `current` tinyint(1) NOT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `server_version` varchar(20) DEFAULT NULL,
  `response_code` int(11) DEFAULT NULL,
  `response_time` int(11) DEFAULT NULL,
  `measured_from` varchar(100) NOT NULL,
  `measured_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `hostname` (`hostname`,`current`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
