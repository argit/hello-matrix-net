#!/bin/sh

# This script requires an installed version of ssllabs-scan from
# https://github.com/ssllabs/ssllabs-scan

SSLLABS=/your/path/to/ssllabs-scan
CURL=/usr/bin/curl
GREP=/bin/grep
ECHO=/bin/echo
DATE=/bin/date
TEMPPATH=/your/temp/path/temp_results.txt
SLEEP=/bin/sleep

LIST_URL=https://www.hello-matrix.net/public_servers.php
SECRET_KEY=enter secret key here


# Print date
$DATE

# Loop through servers
$CURL -s $LIST_URL?format=plain | \
  $GREP -o -E "^[a-zA-Z0-9\.:-]+" | \
   while read SERVER
   do
    $ECHO "Checking $SERVER..."
   
    $SSLLABS -usecache=true -maxage=48 $SERVER > $TEMPPATH
    
    echo "DATA: 000 0.000 0.000" >> $TEMPPATH
    echo "DATA: $SERVER" >> $TEMPPATH
    echo "DATA: $SECRET_KEY" >> $TEMPPATH
    echo "DATA: SSLLABS_RESULTS" >> $TEMPPATH
    
    $CURL -s --max-time 15 -X POST --data-binary @- $LIST_URL < $TEMPPATH
    
    rm $TEMPPATH
    
    # We sleep 60 seconds to be nice to SSLLabs.
    $SLEEP 60
    
   done
   
$ECHO Done.

# Print newlines
$ECHO 
$ECHO 

