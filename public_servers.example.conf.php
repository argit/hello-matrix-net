<?

/* MySQL database configuration to use */
$db_host = "database host here";
$db_name = "database name here";
$db_user = "database user here";
$db_pass = "database password here";

/* Secret keys used by the reporting servers for authenticating statistical reporting received */
$secret_keys = array(
   "secret key here" => "location name here"
);

/* Secret key for matrix-monitor-bot */
$monitor_data_key = "secret key here";

/* Secret key for starting the statistics recalculation */
$recalculate_secret_key = "other secret key here";

/* Which is the default location we should show when the user doesn't select anything else? */
$default_show_from = "location name here (must match one of the above)";

/* Target email address for global warnings (public servers with disabled registration, timing reporters that stop responding) */
$warnings_email = "your email here";

?>