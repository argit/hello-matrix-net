<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Unofficial selection of public Matrix servers</title>
	
		<style>
		 body {
		   font-family: Helvetica, Arial, Helvetica, sans-serif;
		   font-size: 10pt;
		   margin: 5%;
		 } 
		 
		 a {
		   text-decoration: none;
		 }
		 
		 a:hover {
		   text-decoration: underline;
		 }
		 
		 table {
		   width: 100%;
		 }
		 
		 td,th {
		   padding-left: 5px;
		   padding-right: 5px;
		 }
		 
		 .server-list {
		   font-size: 10pt;
		 }
		 
		 .server-list a {
       color: black;
		 }
		 
		 .table-code {
		   font-family: Courier New, Courier, fixed-width;
		 }
		 
		 .table-num {
		   text-align: right;
		 }
		 
		 .table-indicator {
		   text-align: center;
		 }
		 
		 .table-footer-right {
		   text-align: right;
		   font-size: small;
		   font-style: italic;
		   padding: 10px;
		 }
		 
		 .ssl-grade-green a {
		   font-weight: bold;
		   color: #4EC83D;
		 }
		 
		 .ssl-grade-orange a {
		   font-weight:bold;
		   color: #FFA100;
		 }
		 
		 .ssl-grade-red a {
		   font-weight:bold;
		   color: #EF251E;
		 }
		 
		 li {
		   margin-bottom: 10px; 
		 }
		 
		 .location-selector {
		   font-size: small;
		   float: right;
		   margin-left: 15px;
		   margin-bottom: 15px;
		 }
		 
		 .server-online {
		   color: #4EC83D;
		 }
		 
		 .server-offline {
		   color: #EF251E;
		 }
		 
		 .notice {
		 	margin-top: 15px;
		 	margin-bottom: 15px;
		 	margin-left: auto;
		 	margin-right: auto;
		 	max-width: 700px;
		 	
		 	border-width: 4px;
		 	border-style: solid;
		 	border-color: #880000;
		 	
		 	padding: 10px 20px 10px 20px;
		 }
		 
		 .server-entry {
       margin: 5%;
       padding: 10px;
       border: lightgrey solid 1px;
       box-shadow: 0 5px 5px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
     }
     
     .server-heading {
       font-size: 12pt;
       font-weight: normal;
     }
     
     .server-hostname {
       font-family: Courier New, Courier;
       font-size: 11pt;
     }
     
     .server-comments {
       margin: 20px;
      font-size: 10pt;
     }
     
     .server-comments-pointer {
       color: lightgrey;
       font-size: 8pt;
     }
     
 		 
		 .footer {
		   font-size: small;
		   text-align: center;
		 }
		 
		 @media screen and (max-width: 900px) {
		   .hide-on-small {  
		    display: none;
		   }
		   
		   body {
		    margin-left: 1%;
		    margin-right: 1%;
		   }
		 }
		 
		 @media screen and (max-width: 650px) {
		   .hide-on-tiny {
		     display: none;
		   }
		 }
		
		</style>
	</head>
<body>
<h1>Unofficial selection of public Matrix servers</h1>
 
<p>This list provides a selection of public Matrix homeservers which you can register on to join the Matrix universe. This is <emph>not</emph> a comprehensive list of homeservers but instead a highly subjective list of hand-picked homeservers that might be a good fit for newcomers to Matrix. There are thus also no clear criteria for inclusion on this list, although we prioritize stable, general-purpose servers run by organisations with a track record and a focus on open, federated comunication. If you believe your public homeserver would be a good fit for inclusion on the list or for any other questions, get in touch in <a href="https://matrix.to/#/#hello-matrix:matrix.org">#hello-matrix:matrix.org</a>.</p>



<?php foreach($public_servers as $server) { ?>
   <div class="server-entry">
    <h2 class="server-heading">
      <span class="server-hostname"><?=$server['hostname']?></span> - 
      <?=($server['url'] ? '<a href="'.$server['url'].'">' : '')?>
      <?=$server['description']?>
      <?=($server['url'] ? '</a>' : '')?>
    </h2>
   
    <table class="server-list">
      <thead>
        <tr>
         <th>
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Country' ? '' : 'Country')?>">Country</a>
           <?=($sortby=='Country' ? ' ▲' : '')?>
         </th>
         <th class="hide-on-small">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Since' ? '' : 'Since')?>">Since</a>
           <?=($sortby=='Since' ? ' ▲' : '')?>
         </th>
         <th class="hide-on-small">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Software' ? '' : 'Software')?>">Software</a>
           <?=($sortby=='Software' ? ' ▲' : '')?>
         </th>
         <th>
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Last Status' ? '' : 'Last%20Status')?>">Last Status</a>
           <?=($sortby=='Last Status' ? ' ▲' : '')?>
         </th>
         <th class="hide-on-tiny">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Reliability' ? '' : 'Reliability')?>">Reliability</a>
           <?=($sortby=='Reliability' ? ' ▼' : '')?>
         </th>
         <th class="hide-on-small">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Avg Response' ? '' : 'Avg%20Response')?>">Avg Response</a>
           <?=($sortby=='Avg Response' ? ' ▲' : '')?>
         </th>
         <th class="hide-on-small">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Federation' ? '' : 'Federation')?>">Federation</a>
           <?=($sortby=='Federation' ? ' ▲' : '')?>
         </th>
         <th class="hide-on-tiny">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Users' ? '' : 'Users')?>">Users</a>
           <?=($sortby=='Users' ? ' ▼' : '')?>
         </th>
         <th class="hide-on-small">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Public Aliases' ? '' : 'Public%20Aliases')?>">Public Aliases</a>
           <?=($sortby=='Public Aliases' ? ' ▼' : '')?>
         </th>
         <th class="hide-on-tiny">
           <a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='SSL Labs' ? '' : 'SSL%20Labs')?>">SSL Labs</a>
           <?=($sortby=='SSL Labs' ? ' ▼' : '')?>
         </th>
        </tr>
      </thead>
      <tbody>
        <tr>
         <td class="table-indicator"><?=$server['location']?></td>
         <td class="table-indicator hide-on-small"><?=$server['online_since']?></td>
         <td class="table-text hide-on-small" title="<?=($server['server_name'] ? $server['server_name']." ".$server['server_version'] : $server['server'])?>"><?=($server['server_name'] ? $server['server_name']." ".(strlen($server['server_version'])>8 ? substr($server['server_version'], 0, 8).'..' : $server['server_version']) : $server['server'])?></td>
         <td class="table-indicator <?=($server['last_response'] == 200 ? "server-online" : "server-offline")?>" title="<?=($server['last_response'] == 200 ? "Online" : "Offline")." since ".gmdate('Y-m-d H:i', strtotime($server['status_since']))?>"><?=($server['last_response'] == 200 ? "Online" : "Offline")?></td>
         <td class="table-num hide-on-tiny" title="Shows 14 days. Last hour: <?=($server['measurements_short'] > 0 ? number_format($server['successful_short'] * 100 / $server['measurements_short'], 1)." %" : "n/a") ?>"><?=($server['measurements'] > 0 ? number_format($server['successful'] * 100 / $server['measurements'], 1)." %" : "n/a") ?></td>
         <td class="table-num hide-on-small" title="Shows 14 days. Last hour: <?=($server['successful_short'] > 0 ? number_format($server['sum_response_time_short'] / $server['successful_short'], 0)." ms" : "n/a")?>"><?=($server['successful'] > 0 ? number_format($server['sum_response_time'] / $server['successful'], 0)." ms" : "n/a")?></td>
         <td class="table-num hide-on-small" title="Shows 14 days. Last hour: <?=($server['sendPingShort'] + $server['receivePingShort'] > 0 ? number_format(($server['sendPingShort'] + $server['receivePingShort']) * 1000 / 2, 0)." ms" : "n/a")?>"><?=(($server['sendPingLong'] + $server['receivePingLong']) > 0 ? number_format(($server['sendPingLong'] + $server['receivePingLong']) * 1000 / 2, 0)." ms" : "n/a")?></td>
         <td class="table-num hide-on-tiny"><?=($server['users_active'] ? number_format($server['users_active'], 0) : 'n/a')?></td>
         <td class="table-num hide-on-small"><?=($server['public_room_count'] ? number_format($server['public_room_count'], 0) : 'n/a')?></td>
         <td class="table-indicator hide-on-tiny ssl-grade-<?=((!$server['hasWarnings'] && substr($server['grade'], 0, 1) == 'A') ? 'green' : ($server['grade'] == 'F' || $server['grade'] == 'T' ? 'red' : 'orange'))?>" data-sort="<?=(isset($sslGradeMap[$server['grade']]) ? $sslGradeMap[$server['grade']] : 1000)?>"><a href="https://www.ssllabs.com/ssltest/analyze.html?d=<?=$server['hostname']?>" target="_blank"><?=($server['grade'] ? $server['grade'].($server['hasWarnings'] ? " (!)" : "") : "n/a")?></a></td>
        </tr> 
      </tbody>
    </table>
    
    <p class="server-comments">
      <span class="server-comments-pointer">▶</span>︎ <?=$server['comments']?>
    </p>
  </div>        
<?php } ?>


<p>The table provides the following information:</p>

<form action="public_servers.php" method="GET">
  <input type="hidden" name="sortby" value="<?=$sortby?>">
  <ul>
   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Software' ? '' : 'Software')?>"><b>Software</b><?=($sortby=='Software' ? ' ▲' : '')?></a> shows the software the server is running, as returned by the server's <code>/_matrix/federation/v1/version</code> endpoint.</li>

   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Last Status' ? '' : 'Last%20Status')?>"><b>Last Status</b><?=($sortby=='Last Status' ? ' ▲' : '')?></a> shows whether the last poll on the <code>/_matrix/client/versions</code> endpoint was successful ("Online") or unsuccessful ("Offline"). We poll every five minutes. If you hover over the item it shows when the status last changed. Times are given in UTC.</li>

   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Reliability' ? '' : 'Reliability')?>"><b>Reliability</b><?=($sortby=='Reliability' ? ' ▼' : '')?></a> is based on the same polling. It shows the percentage of requests that were successful (returned with status code 200) over the last 14 days the server was listed.</li>

   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Avg Response' ? '' : 'Avg%20Response')?>"><b>Avg Response</b><?=($sortby=='Avg Response' ? ' ▲' : '')?></a> shows the average time it took to poll the above. There are multiple locations for measuring response times available. You have currently selected timings from

     <select name="show_from" onchange="this.form.submit()">
       <?php foreach($secret_keys as $secret_key => $location) { ?>
         <option<?=($location == $show_from ? ' selected' : '')?>><?=$location?></option>
       <?php } ?>
      </select>
      <noscript>
       <input type="submit" value="Go">
      </noscript>
      
      . In general, you should choose a timing source that is as close as possible to you (ideally at least on the same continent). Mouseover will display the average for the last hour.</li>

   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Federation' ? '' : 'Federation')?>"><b>Federation</b><?=($sortby=='Federation' ? ' ▲' : '')?></a> measures the time it takes for a message from this homeserver to reach other servers and vice-versa. It is calculated using the <a href="https://github.com/turt2live/matrix-monitor-bot">matrix-monitor-bot</a> project as the total ping duration (<code>monbot_ping_time</code>) for each server. A homeserver needs to run a matrix-monitor-bot in the #monitor-public:matrix.org room for data to be gathered.</li>

   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Users' ? '' : 'Users')?>"><b>Users</b><?=($sortby=='Users' ? ' ▼' : '')?></a> are self-reported numbers and only available for servers whose operators  are providing us with these numbers.</li>

   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='Public Aliases' ? '' : 'Public%20Aliases')?>"><b>Public Aliases</b><?=($sortby=='Public Aliases' ? ' ▼' : '')?></a> shows the number of published aliases in that homeserver's public room directory and is based on the `total_room_count_estimate` returned by the servers' APIs. We update this number once a day.</li>

   <li><a href="public_servers.php?show_from=<?=urlencode($show_from)?>&amp;sortby=<?=($sortby=='SSL Labs' ? '' : 'SSL%20Labs')?>"><b>SSL Labs</b><?=($sortby=='SSL Labs' ? ' ▼' : '')?></a> shows the most recent rating of the SSL/TLS configuration of the server's client interface according to <a href="https://www.ssllabs.com/ssltest/">Qualys' SSL Labs</a> (updated about once a week).</li>
  </ul>
</form>

<p>Servers are displayed in random order. You can order by a specific data item by clicking on the relevant header in the server overview.</p>


<div class="table-footer-right">
  Last updated: <?=gmdate('Y-m-d H:i', strtotime($last_timing))?> UTC
</div>

<hr>

<p class="footer">[ <a href="public_servers.php?format=json&amp;only_public=true&amp;show_from=<?=urlencode($show_from)?>">JSON of all servers</a> ]</p>


</body>
</html>

<!--
  Before database: <?=($timing_pre_database-$timing_start)?>ms
  Before last timing: <?=($timing_pre_last_timing-$timing_pre_database)?>ms
  Before template: <?=($timing_pre_template-$timing_pre_last_timing)?>ms
  Until now: <?=(microtime(true)*1000-$timing_pre_template)?>ms
-->
